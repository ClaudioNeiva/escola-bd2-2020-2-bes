package br.ucsal.bes20202.bd2.escola;

import javax.persistence.Embeddable;

@Embeddable
public class Telefone {

	private Integer ddi;

	private Integer ddd;

	private TipoTelefoneEnum tipo;

	private String numero;

	public Telefone() {
	}

	public Telefone(Integer ddi, Integer ddd, TipoTelefoneEnum tipo, String numero) {
		super();
		this.ddi = ddi;
		this.ddd = ddd;
		this.tipo = tipo;
		this.numero = numero;
	}

	public Integer getDdi() {
		return ddi;
	}

	public void setDdi(Integer ddi) {
		this.ddi = ddi;
	}

	public Integer getDdd() {
		return ddd;
	}

	public void setDdd(Integer ddd) {
		this.ddd = ddd;
	}

	public TipoTelefoneEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoTelefoneEnum tipo) {
		this.tipo = tipo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}
