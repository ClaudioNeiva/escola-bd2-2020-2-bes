package br.ucsal.bes20202.bd2.escola;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.ucsal.bes20202.bd2.escola.dtos.ResultadoDTO;

public class Exemplo {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula02jpa");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Uf ufSP = new Uf("SP", "São Paulo");
		// em.persist(ufSP);

		Cidade cidadeSAO = new Cidade(ufSP, "SAO", "São Paulo");
		Cidade cidadeGRU = new Cidade(ufSP, "GRU", "Guarulhos");
		Cidade cidadeSBC = new Cidade(ufSP, "SBC", "São Bernardo do Campo");
		em.persist(cidadeSAO);
		em.persist(cidadeGRU);
		em.persist(cidadeSBC);

		// Bidirecional?
		// Cascade?

		Curso cursoBES = new Curso("BES", "Bacharelado em Engenharia de Software", LocalDate.of(2014, 01, 05));
		em.persist(cursoBES);

		Discinte alunoClaudio = new Discinte("Claudio Neiva", SituacaoAlunoEnum.FORMADO);
		alunoClaudio.setEndereco(new Endereco("Rua 1", "123", "Pituaçu", cidadeSAO));
		alunoClaudio.getCursos().add(cursoBES);
		em.persist(alunoClaudio);

		AlunoExterno alunoPedro = new AlunoExterno("Pedro", SituacaoAlunoEnum.ATIVO, "UFBA");
		alunoPedro.setEndereco(new Endereco("Rua 2", "456", "Pituaçu", cidadeGRU));
		alunoClaudio.getCursos().add(cursoBES);
		em.persist(alunoPedro);

		AlunoExterno alunoJoaquim = new AlunoExterno("Joaquim", SituacaoAlunoEnum.ATIVO, "UFBA");
		alunoJoaquim.setEndereco(new Endereco("Rua 3", "867", "Brotas", cidadeSAO));
		alunoClaudio.getCursos().add(cursoBES);
		em.persist(alunoJoaquim);

		em.getTransaction().commit();

		System.out.println("*************************************1");
		em.clear();
		String oql = "select a from Aluno a where TYPE(a) = Aluno and a.endereco.bairro = :bairro order by a.endereco.cidade.uf.nome asc";
		TypedQuery<Discinte> query = em.createQuery(oql, Discinte.class);
		query.setParameter("bairro", "Pituaçu");
		List<Discinte> alunos = query.getResultList();

		System.out.println("Alunos que moram em Pituaçu:");
		alunos.stream().forEach(a -> System.out.println(a.getTelefones()));

		System.out.println("*************************************2");

		String oql2 = "select a.situacao, count(*) from Aluno a group by a.situacao";
		TypedQuery<Object[]> query2 = em.createQuery(oql2, Object[].class);
		List<Object[]> situacoesQtds = query2.getResultList();
		System.out.println("Quantidade de alunos por situação:");
		situacoesQtds.stream().forEach(qas -> System.out.println(qas[0] + " -> " + qas[1]));

		System.out.println("*************************************3");

		String oql3 = "select new " + ResultadoDTO.class.getCanonicalName()
				+ "(a.situacao, count(*)) from Aluno a group by a.situacao";
		TypedQuery<ResultadoDTO> query3 = em.createQuery(oql3, ResultadoDTO.class);
		List<ResultadoDTO> resultadosDTO = query3.getResultList();
		System.out.println("Quantidade de alunos por situação:");
		resultadosDTO.stream().forEach(System.out::println);

		System.out.println("*************************************4");
		em.clear();
		String oql4 = "select u from Uf u left outer join fetch u.cidades";
		TypedQuery<Uf> query4 = em.createQuery(oql4, Uf.class);
		List<Uf> ufs = query4.getResultList();
		System.out.println("*************************************4a");
		// em.close();
		// emf.close();
		System.out.println("ufs=" + ufs.get(0).getCidades());

		System.out.println("*************************************5");
		em.clear();
		TypedQuery<Discinte> query5 = em.createNamedQuery("alunos por situação", Discinte.class);
		query5.setParameter("situacao", SituacaoAlunoEnum.ATIVO);
		List<Discinte> alunos5 = query5.getResultList();
		System.out.println("Alunos por ordem de nome:");
		alunos5.stream().forEach(System.out::println);

		System.out.println("*************************************6");
		em.clear();
		String sql = "select a.nome, c.sigla from tab_aluno a inner join tab_cidade c on (a.cidade_sigla = c.sigla)";
		Query query6 = em.createNativeQuery(sql);
		@SuppressWarnings("unchecked")
		List<Object[]> results = query6.getResultList();
		for (Object[] result : results) {
			System.out.println(result[0] + "|" + result[1]);
		}

		em.close();
		emf.close();
	}

}
