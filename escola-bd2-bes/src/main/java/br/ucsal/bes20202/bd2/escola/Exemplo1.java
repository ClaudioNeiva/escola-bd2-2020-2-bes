package br.ucsal.bes20202.bd2.escola;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo1 {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula02jpa");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Uf ufSP = new Uf("SP", "São Paulo");
		em.persist(ufSP);

		Cidade cidadeSAO = new Cidade(ufSP, "SAO", "São Paulo");
		Cidade cidadeGRU = new Cidade(ufSP, "GRU", "Guarulhos");
		Cidade cidadeSBC = new Cidade(ufSP, "SBC", "São Bernardo do Campo");
		em.persist(cidadeSAO);
		em.persist(cidadeGRU);
		em.persist(cidadeSBC);

		em.getTransaction().commit();

		em.clear();
		ufSP = em.find(Uf.class, "SP");

		System.out.println("******************************");
		System.out.println("ufSP=" + ufSP);
		ufSP.getCidades().forEach(System.out::println);

		System.out.println("******************************");
		
		em.getTransaction().begin();
		ufSP.getCidades().remove(2);
		em.getTransaction().commit();

		em.clear();
		ufSP = em.find(Uf.class, "SP");
		System.out.println("ufSP=" + ufSP);
		ufSP.getCidades().forEach(System.out::println);

		em.close();
		emf.close();
	}

}
