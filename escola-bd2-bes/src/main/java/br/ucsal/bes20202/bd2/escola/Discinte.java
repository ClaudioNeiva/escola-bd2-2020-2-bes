package br.ucsal.bes20202.bd2.escola;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@Entity(name = "Aluno")
@Table(name = "tab_aluno", uniqueConstraints = { @UniqueConstraint(name = "un_aluno_cpf", columnNames = { "cpf" }) })
@SequenceGenerator(name = "sq_aluno", sequenceName = "seq_aluno")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "tipo", length = 3, discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("ALN")
@NamedQuery(query = "select a from Aluno a where a.situacao = :situacao order by a.nome", name = "alunos por situação")
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
public class Discinte implements Serializable {

	private static final long serialVersionUID = 1L;

	// int
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_aluno")
	private Integer matricula;

	// varchar(40)
	@Column(length = 40)
	private String nome;

	// char(11)
	private String cpf;

	@ManyToMany
	@JoinTable(name = "tab_aluno_curso", joinColumns = { @JoinColumn(name = "matricula_aluno") }, inverseJoinColumns = {
			@JoinColumn(name = "codigo_curso") })
	private List<Curso> cursos;

	// char(3)
	// @Convert(converter = SituacaoAlunoConverter.class)
	// @Column(columnDefinition = "char(3)")
	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "situacao_aluno_t")
	@Type(type = "pgsql_enum")
	private SituacaoAlunoEnum situacao;

	// numeric(10,2)
	@Column(name = "renda_familiar", precision = 10, scale = 2)
	private BigDecimal rendaFamiliar;

	@Transient
	private Long numRg;

	@Transient
	private String orgaoExpedidor;

	@Transient
	private Uf ufOrgaoExpedidor;

	// cada telefone deve ser varchar(15)
	@ElementCollection
	@Column(length = 15)
	private List<Telefone> telefones;

	@ElementCollection
	private List<Responsavel> responsaveis;

	@Embedded
	private Endereco endereco;

	public Discinte() {
		super();
	}

	public Discinte(String nome, SituacaoAlunoEnum situacaoAlunoEnum) {
		this.nome = nome;
		this.situacao = situacaoAlunoEnum;
	}

	public Discinte(String nome, Curso curso, SituacaoAlunoEnum situacao) {
		this(nome, situacao);
		this.cursos = new ArrayList<>();
		this.cursos.add(curso);
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public List<Curso> getCursos() {
		if (cursos == null) {
			cursos = new ArrayList<>();
		}
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public SituacaoAlunoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAlunoEnum situacao) {
		this.situacao = situacao;
	}

	public BigDecimal getRendaFamiliar() {
		return rendaFamiliar;
	}

	public void setRendaFamiliar(BigDecimal rendaFamiliar) {
		this.rendaFamiliar = rendaFamiliar;
	}

	public Long getNumRg() {
		return numRg;
	}

	public void setNumRg(Long numRg) {
		this.numRg = numRg;
	}

	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}

	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}

	public Uf getUfOrgaoExpedidor() {
		return ufOrgaoExpedidor;
	}

	public void setUfOrgaoExpedidor(Uf ufOrgaoExpedidor) {
		this.ufOrgaoExpedidor = ufOrgaoExpedidor;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", cpf=" + cpf + ", situacao=" + situacao
				+ ", rendaFamiliar=" + rendaFamiliar + ", numRg=" + numRg + ", orgaoExpedidor=" + orgaoExpedidor
				+ ", ufOrgaoExpedidor=" + ufOrgaoExpedidor + ", telefones=" + telefones + ", endereco=" + endereco
				+ "]";
	}

}
